"use strict";

const got = require("got");

const lib = require("../dist/lib");
const { Options } = require("../dist/lib/options");

describe("REST API lib", () => {
  beforeAll(async () => {
    Options.TotalBots = 0;
    await lib.main();
  });

  afterAll(async () => {
    await lib.clean();
  });

  test("GET /room returns rooms", async () => {
    const url = new URL("/room", Options.HTTP.Url);
    const response = (await got.get(url, { json: true })).body;
    expect(response).toMatchObject({
      httpCode: 200,
      errors: [],
      rooms: expect.any(Array)
    });
  });
});
