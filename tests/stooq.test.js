"use strict";

const { Stooq } = require("../dist/lib/daos/stooq");

describe("Stooq API DAO", () => {
  const dao = new Stooq();
  test("it can get a candle (JSON format)", async () => {
    const candle = await dao.getCandle("aapl.us");
    expect(candle).toMatchObject({
      symbol: "AAPL.US",
      close: expect.any(Number)
    });
  });
});
