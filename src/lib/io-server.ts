import * as SocketIO from "socket.io";

import logger from "./logger";
import { Options } from "./options";

class IoServer {
  private server: SocketIO.Server | undefined;
  private namespace: SocketIO.Namespace | undefined;

  connect(): void {
    this.server = SocketIO(Options.IO.Port, { serveClient: true });
    this.namespace = this.server.of("/financial-chat");
    logger.info("Socket.IO server up.");
  }

  disconnect(): Promise<void> {
    return new Promise((resolve): void => {
      if (!this.server) {
        return resolve();
      }
      this.server.close(resolve);
    });
  }

  emit(event: string, message: unknown): void {
    if (this.namespace) {
      this.namespace.emit(event, message);
    }
  }
}

export default new IoServer();
