import * as expressSession from "express-session";
import * as connectMongo from "connect-mongo";
import { MongoClient } from "mongodb";

import { Options } from "./options";
import { daos, ConnectionSource } from "./daos";

const MongoStoreFactory = connectMongo(expressSession);

class Session {
  private store: connectMongo.MongoStore | undefined;

  private cookie = {
    domain:
      Options.Environment === "production"
        ? Options.HTTP.CookieDomain
        : undefined,
    path: "/",
    httpOnly: true,
    secure: Options.Environment === "production",
    sameSite: "lax" as "lax", // `sameSite` accepts specific strings (string literal types) in `ExpressSession.SessionOptions`
    maxAge: 1 * 60 * 60 * 1000 // i.e. 1 hour in milliseconds.
  };

  public options = {
    cookie: this.cookie,
    resave: false,
    saveUninitialized: false,
    secret: Options.HTTP.CookieSecret,
    name: Options.HTTP.CookieName,
    store: this.store
  };

  plug(): void {
    this.options.store = new MongoStoreFactory({
      client: daos.getSource(ConnectionSource.Mongo) as MongoClient,
      ttl: 1 * 60 * 60, // i.e. 1 hour in seconds.
      autoRemove: "native",
      collection: "session",
      stringify: false
    });
  }
}

const session = new Session();
export { session };
