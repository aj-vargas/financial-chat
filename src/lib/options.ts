import { config } from "dotenv";

config();

class Options {
  static readonly Environment = process.env.NODE_ENV || "development";
  static readonly LogTimezone = process.env.LOG_TIMEZONE || "UTC";
  static TotalBots = process.env.TOTAL_BOTS ? +process.env.TOTAL_BOTS : 2;
  static readonly HTTP = {
    Hostname: process.env.HTTP_HOSTNAME || "localhost",
    Port: process.env.HTTP_PORT ? +process.env.HTTP_PORT : 8080,
    Url: process.env.HTTP_URL || "http://localhost:8080/",
    CookieSecret: process.env.HTTP_COOKIE_SECRET || "",
    CookieName: process.env.HTTP_COOKIE_NAME || "",
    CookieDomain: process.env.HTTP_COOKIE_DOMAIN || ""
  };
  static readonly IO = {
    Port: process.env.IO_PORT ? +process.env.IO_PORT : 49159
  };
  static readonly Auth0 = {
    Domain: process.env.AUTH0_DOMAIN || "",
    ClientId: process.env.AUTH0_CLIENT_ID || "",
    ClientSecret: process.env.AUTH0_CLIENT_SECRET || "",
    CallbackPath: process.env.AUTH0_CALLBACK_PATH || "",
    SignOutPath: process.env.AUTH0_SIGNOUT_PATH || "",
    Scope: process.env.AUTH0_SCOPE || "read:users"
  };
  static readonly CloudAMQP = {
    Url: process.env.CLOUDAMQP_URL || "",
    StockQueue: process.env.CLOUDAMQP_STOCK_QUEUE || "",
    AnswerQueue: process.env.CLOUDAMQP_ANSWER_QUEUE || ""
  };
  static readonly Mongo = {
    ClusterUrl: process.env.MONGO_CLUSTER_URL || ""
  };
}

export { Options };
