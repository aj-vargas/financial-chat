import { spawn, ChildProcessWithoutNullStreams } from "child_process";

import logger from "./logger";
import { Options } from "./options";

function log(error: Error): void {
  logger.error("Error received from bot process.", error);
}

class BotPool {
  private bots: ChildProcessWithoutNullStreams[] = [];

  fill(): void {
    for (let i = 0; i < Options.TotalBots; ++i) {
      const bot = spawn("node", ["dist/bin/bot.js"]);
      bot.on("error", log);
      logger.info(`Bot spawned. Its PID is ${bot.pid}.`);
      this.bots.push(bot);
    }
  }

  kill(): void {
    for (const bot of this.bots) {
      bot.off("error", log);
      // TODO: It might kill something other than the bot. Fix it.
      // https://nodejs.org/api/child_process.html#child_process_subprocess_kill_signal
      bot.kill("SIGTERM");
    }
  }
}

export default new BotPool();
