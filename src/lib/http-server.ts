import * as http from "http";
import * as express from "express";

import { Options } from "./options";
import routes from "./routes";
import logger from "./logger";

class HttpServer {
  private app = express();
  private server: http.Server | undefined;

  constructor() {
    if (Options.Environment === "production") {
      this.app.set("trust proxy", "loopback");
    }
    this.app.set("view engine", "pug");
    this.app.set("views", "./dist/views");
    this.app.locals.basedir = "./dist/views"; // To use absolute paths with Pug.
    logger.info("Express app loaded.");
  }

  connect(): void {
    routes.build();
    this.app.use(routes.router);
    this.server = this.app.listen(Options.HTTP.Port, Options.HTTP.Hostname);
    logger.info("HTTP server up.");
  }

  disconnect(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (!this.server) {
        return resolve();
      }
      this.server.close(err => (err ? reject(err) : resolve()));
    });
  }
}

export default new HttpServer();
