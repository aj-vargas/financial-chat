import { Db, Collection, ObjectId } from "mongodb";

interface User {
  _id?: string;
  email: string;
  handle: string;
  profiles: object;
}

interface LogEntry {
  _id?: string;
  provider: string;
  ip: string;
  agent: string;
}

class AppUser {
  private collection: Collection;

  constructor(database: Db) {
    this.collection = database.collection("user");
  }

  /**
   * Gets the user linked to a provider profile.
   *
   * @param provider The provider of the profile.
   * @param id The profile identifier.
   *
   * @return The user linked to the provider profile.
   */
  async getByProfileId(provider: string, id: string): Promise<User | null> {
    if (!provider || !id) {
      return null;
    }
    const result = await this.collection.findOne(
      { [`profiles.${provider}`]: id },
      { projection: { signIns: 0 } }
    );
    if (result) {
      result._id = result._id.toString();
    }
    return result;
  }

  /**
   * Gets the user by its internal identifier.
   *
   * @param _id The user internal identifier.
   *
   * @return The user.
   */
  async getByObjectId(_id: string): Promise<User | null> {
    if (!_id) {
      return null;
    }
    const result = await this.collection.findOne(
      { _id: new ObjectId(_id) },
      { projection: { signIns: 0 } }
    );
    if (result) {
      result._id = result._id.toString();
    }
    return result;
  }

  /**
   * Gets the user by its email.
   *
   * @param email The user email.
   *
   * @return The user.
   */
  async getByEmail(email: string): Promise<User | null> {
    if (!email) {
      return null;
    }
    const result = await this.collection.findOne(
      { email },
      { projection: { signIns: 0 } }
    );
    if (result) {
      result._id = result._id.toString();
    }
    return result;
  }

  /**
   * Logs certain data of the sign in for the user.
   *
   * @param entry The log entry to persist.
   * @param entry.provider The provider of the profile used.
   * @param entry._id The internal identifier of the user that signed in.
   * @param entry.ip The alleged IP of the user's machine.
   * @param entry.agent The alleged user's agent, i.e. web browser.
   *
   * @return If the log entry was successfully persisted.
   */
  async log({ provider, _id, ip, agent }: LogEntry): Promise<boolean> {
    const result = await this.collection.findOneAndUpdate(
      { _id: new ObjectId(_id) },
      {
        $push: {
          signIns: {
            $each: [{ at: Date.now(), provider, ip, agent }],
            $sort: { at: -1 },
            $position: 0
          }
        }
      },
      { upsert: false }
    );
    return !!result.value;
  }

  /**
   * Upserts a user.
   *
   * @param user The values for the user.
   * @param user.email The email used for access.
   * @param user.handle The handle for the user.
   * @param user.profiles Dictionary of providers and the corresponding profile identifier for the user.
   *
   * @return The user as is after being upserted.
   */
  async upsert({ email, handle, profiles }: User): Promise<User> {
    if (!email) {
      throw new Error("The field 'email' is required.");
    }
    const $set = { handle, profiles: {} };
    if (profiles) {
      $set.profiles = profiles;
    }
    const result = await this.collection.findOneAndUpdate(
      { email },
      {
        $setOnInsert: {
          email,
          signIns: [],
          createdAt: Date.now()
        },
        $set: {
          ...$set,
          updatedAt: Date.now()
        }
      },
      { upsert: true, returnOriginal: false }
    );
    return result.value;
  }
}

export { AppUser, User, LogEntry };
