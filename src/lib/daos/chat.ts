import { Db, Collection } from "mongodb";

interface ChatMessage {
  _id?: string;
  room: string;
  user: object;
  text: string;
  createdAt?: number;
}

class Chat {
  private collection: Collection;

  constructor(database: Db) {
    this.collection = database.collection("chat");
  }

  async getByRoom(room: string, limit = 50): Promise<ChatMessage[]> {
    const documents = await this.collection
      .aggregate([
        { $match: { room } },
        { $sort: { createdAt: -1 } },
        { $limit: limit },
        { $sort: { createdAt: 1 } },
        {
          $project: {
            _id: { $toString: "$_id" },
            room: 1,
            user: 1,
            text: 1,
            createdAt: 1
          }
        }
      ])
      .toArray();
    return documents;
  }

  async insert(message: ChatMessage): Promise<ChatMessage | null> {
    const result = await this.collection.insertOne({
      createdAt: Date.now(),
      text: message.text,
      room: message.room,
      user: message.user
    });
    return result.insertedCount === 1 ? result.ops[0] : null;
  }
}

export { Chat, ChatMessage };
