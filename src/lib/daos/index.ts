import { ManagementClient } from "auth0";
import { MongoClient, Db, ObjectId } from "mongodb";

import { AuthUser } from "./auth-user";
import { AppUser, User, LogEntry } from "./app-user";
import { Room, ChatRoom } from "./room";
import { Chat, ChatMessage } from "./chat";
import { Stooq, Candle } from "./stooq";
import { Options } from "../options";

interface MongoConnection {
  client: MongoClient | null;
  db: Db | null;
}

interface Auth0Connection {
  client: ManagementClient | null;
}

enum ConnectionSource {
  Auth0,
  Mongo
}

class Daos {
  private mongodb: MongoConnection;
  private auth0: Auth0Connection;

  private _authUser: AuthUser | undefined;
  private _appUser: AppUser | undefined;
  private _room: Room | undefined;
  private _chat: Chat | undefined;
  private _stooq: Stooq | undefined;

  constructor() {
    this.mongodb = {
      client: null,
      db: null
    };
    this.auth0 = {
      client: null
    };
  }

  async connect(): Promise<void> {
    this.auth0.client = new ManagementClient({
      domain: Options.Auth0.Domain,
      clientId: Options.Auth0.ClientId,
      clientSecret: Options.Auth0.ClientSecret,
      scope: Options.Auth0.Scope
    });
    this.mongodb.client = await MongoClient.connect(Options.Mongo.ClusterUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    this.mongodb.db = this.mongodb.client.db();
  }

  disconnect(): Promise<void> {
    if (this.mongodb.client && this.mongodb.client.isConnected()) {
      return this.mongodb.client.close();
    }
    return Promise.resolve();
  }

  getSource(source: ConnectionSource): MongoClient | null {
    switch (source) {
      case ConnectionSource.Auth0: {
        return null;
      }
      case ConnectionSource.Mongo: {
        return this.mongodb.client;
      }
      default: {
        return null;
      }
    }
  }

  getObjectId(): string {
    return new ObjectId().toHexString();
  }

  get auth0User(): AuthUser {
    if (!this.auth0.client) {
      throw new Error("Auth0 connection isn't set.");
    }
    if (!this._authUser) {
      this._authUser = new AuthUser(this.auth0.client);
    }
    return this._authUser;
  }

  get user(): AppUser {
    if (!this.mongodb.db) {
      throw new Error("MongoDB collection isn't set.");
    }
    if (!this._appUser) {
      this._appUser = new AppUser(this.mongodb.db);
    }
    return this._appUser;
  }

  get room(): Room {
    if (!this.mongodb.db) {
      throw new Error("MongoDB collection isn't set.");
    }
    if (!this._room) {
      this._room = new Room(this.mongodb.db);
    }
    return this._room;
  }

  get chat(): Chat {
    if (!this.mongodb.db) {
      throw new Error("MongoDB collection isn't set.");
    }
    if (!this._chat) {
      this._chat = new Chat(this.mongodb.db);
    }
    return this._chat;
  }

  get stooq(): Stooq {
    if (!this._stooq) {
      this._stooq = new Stooq();
    }
    return this._stooq;
  }
}

const daos = new Daos();

export {
  daos,
  ConnectionSource,
  User,
  LogEntry,
  ChatRoom,
  ChatMessage,
  Candle
};
