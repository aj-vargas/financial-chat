import * as got from "got";

interface Candle {
  symbol: string;
  date: string | number;
  time: string | number;
  open: number;
  high: number;
  low: number;
  close: number;
  volume: number;
}

class Stooq {
  async getCandle(
    symbol: string,
    fields = "sd2t2ohlcv",
    format = "json"
  ): Promise<Candle> {
    const url = new URL(
      `/q/l/?s=${symbol}&f=${fields}&e=${format}`,
      "https://stooq.com/"
    );
    const response = ((await got.get(url, { json: true })).body
      .symbols[0] as unknown) as Candle;
    return response;
  }
}

export { Stooq, Candle };
