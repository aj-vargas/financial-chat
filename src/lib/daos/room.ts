import { Db, Collection } from "mongodb";

interface ChatRoom {
  _id?: string;
  name: string;
}

class Room {
  private collection: Collection;

  constructor(database: Db) {
    this.collection = database.collection("room");
  }

  async getAll(): Promise<ChatRoom[]> {
    const documents = await this.collection
      .aggregate([
        { $match: {} },
        { $sort: { name: 1 } },
        {
          $project: {
            _id: { $toString: "$_id" },
            name: 1
          }
        }
      ])
      .toArray();
    return documents;
  }
}

export { Room, ChatRoom };
