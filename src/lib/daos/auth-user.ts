import * as Auth0 from "auth0";

class AuthUser {
  private client: Auth0.ManagementClient;

  constructor(client: Auth0.ManagementClient) {
    this.client = client;
  }

  /**
   * Returns the owner of the provided email.
   *
   * @param email The email to search for.
   *
   * @return The user with the provided email.
   */
  async getByEmail(
    email: string
  ): Promise<Auth0.User<Auth0.AppMetadata, Auth0.UserMetadata>> {
    const result = await this.client.getUsersByEmail(email);
    return result[0];
  }
}

export { AuthUser };
