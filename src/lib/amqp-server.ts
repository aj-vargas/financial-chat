import * as amqp from "amqplib";

import logger from "./logger";
import { Options } from "./options";

class AmqpServer {
  private connection: amqp.Connection | undefined;
  private channel: amqp.Channel | undefined;

  async connect(queues: string[]): Promise<void> {
    this.connection = await amqp.connect(Options.CloudAMQP.Url);
    this.channel = await this.connection.createChannel();

    const promises = [];
    for (const queue of queues) {
      promises.push(this.channel.assertQueue(queue, { durable: false }));
    }
    await Promise.all(promises);
    logger.info("AMQP server up.");
  }

  disconnect(): Promise<void> {
    if (this.connection) {
      return this.connection.close();
    }
    return Promise.resolve();
  }

  enqueue(queue: string, content: Buffer): void {
    if (this.channel) {
      this.channel.sendToQueue(queue, content, { persistent: false });
    }
  }

  dequeue<T>(from: string, to: (content: T) => void): void {
    if (!this.channel) {
      return;
    }
    this.channel.consume(
      from,
      message => {
        try {
          const content = message?.content.toString();
          if (!content) {
            throw new Error("No content.");
          }
          const request = JSON.parse(content);
          if (message) {
            this.channel?.ack(message);
          }
          return to(request);
        } catch (error) {
          logger.error("Couldn't consume message.", error);
          if (message) {
            this.channel?.nack(message, false, false);
          }
        }
      },
      { noAck: false }
    );
  }
}

export default new AmqpServer();
