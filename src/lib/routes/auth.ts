import { URL } from "url";
import * as express from "express";
import * as passport from "passport";
import { Strategy as Auth0Strategy } from "passport-auth0";

import wrap from "../routes/wrap";
import { Options } from "../options";
import { daos, User, LogEntry } from "../daos";
import logger from "../logger";
import { session } from "../session";

const router = express.Router(); /* eslint-disable-line new-cap */

function authentify(
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
): express.Response | void {
  if (request.isAuthenticated()) {
    response.locals.view.user = request.user;
    return next();
  }
  if (request.accepts("html")) {
    if (request.session) {
      request.session.returnUrl = request.originalUrl;
    }
    response.locals.view.returnUrl = request.originalUrl;
    return response.render("sign-in", response.locals.view);
  }
  if (request.accepts("json")) {
    return response.status(403).json({
      httpCode: 403,
      errors: ["You need to sign in."],
      redirectTo: "/sign-in/auth0"
    });
  }
  response.status(406).end();
}

// TODO: Remove this leftover from migration or adapt to the project.
function viewify(
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
): void {
  response.locals.view = {
    ...response.locals.view,
    errors: [],
    returnUrl: request.originalUrl
  };
  next();
}

function userify(
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
): void {
  if (response.locals.view) {
    response.locals.view.user = request.user;
  }
  next();
}

// Every strategy has its own arguments for the verify callback, thus to
// homogenize behaviour: verify won't be called by the strategy itself but a
// callback that will do the proper modifications for what verify needs.
type GetDone = (error: Error | null, user?: User) => void;
async function verify(
  provider: string,
  profile: passport.Profile,
  cb: GetDone
): Promise<void> {
  try {
    if (!profile || !profile.id) {
      return cb(new Error("Credential not provided by provider."));
    }
    let user = await daos.user.getByProfileId(provider, profile.id);
    if (!user) {
      // User of provider doesn't exist as an app user, create it.
      user = {
        email: profile.emails ? profile.emails[0].value : "",
        handle: profile.username || profile.displayName,
        profiles: { [provider]: profile.id }
      };
      user = await daos.user.upsert(user);
      return cb(null, user);
    }
    return cb(null, user);
  } catch (error) {
    return cb(error);
  }
}
async function deserializeUser(_id: string, cb: GetDone): Promise<void> {
  try {
    const user = await daos.user.getByObjectId(_id);
    if (!user) {
      return cb(null);
    }
    return cb(null, user);
  } catch (error) {
    return cb(error);
  }
}

// Some providers keep their own sessions, e.g. Auth0, and to sign the
// user out of them a redirection to a provider specific URL is needed.
function getAfterSignOutUrl(
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
): void {
  switch (request.session?.provider) {
    case "auth0": {
      const url = new URL(`https://${Options.Auth0.Domain}`);
      url.pathname = Options.Auth0.SignOutPath;
      url.searchParams.set("returnTo", Options.HTTP.Url);
      url.searchParams.set("client_id", Options.Auth0.ClientId);
      response.locals.afterSignOutUrl = url.toString();
      break;
    }
    default: {
      response.locals.afterSignOutUrl = "/";
      break;
    }
  }
  next();
}

async function login(
  error: Error,
  user: User,
  request: express.Request,
  response: express.Response
): Promise<void> {
  try {
    if (error) {
      logger.error("Couldn't login authenticated user.", { error });
      response.locals.view.errors.push("Auth couldn't be completed.");
      return response.render("sign-in", response.locals.view);
    }
  } catch (error) {
    logger.error("Couldn't respond to the error as expected.", { error });
    return response.render("error/500");
  }
  try {
    const log: LogEntry = {
      _id: user._id,
      provider: request.session?.provider ?? "",
      ip: request.ip,
      agent: request.headers["user-agent"] || ""
    };
    const wasDone = await daos.user.log(log);
    !wasDone && logger.warn("Didn't log user login.", { log });
  } catch (error) {
    logger.warn("Couldn't log user login.", { error });
  }
  if (!request.session) {
    logger.error("Session unavailable.");
  }
  // An explanation from somebody that also had the problem:
  // https://github.com/jaredhanson/passport/issues/482
  request.session?.save(error => {
    if (error) {
      logger.warn("Couldn't save session for login redirection.", { error });
    }
    response.redirect(request.session?.returnUrl || "/");
  });
}

router.use(passport.initialize());
router.use(passport.session());

passport.serializeUser<User, string>((user, cb) => {
  cb(null, user._id);
});
passport.deserializeUser<User, string>(
  (_id, cb) => deserializeUser(_id, cb) as unknown
);

passport.use(
  new Auth0Strategy(
    {
      domain: Options.Auth0.Domain,
      clientID: Options.Auth0.ClientId,
      clientSecret: Options.Auth0.ClientSecret,
      callbackURL: Options.Auth0.CallbackPath
    },
    (...[, , , profile, cb]) => {
      verify("auth0", profile, cb);
    }
  )
);

router.get("/sign-in/callback", viewify, userify, (request, response, next) => {
  if (!request.session) {
    logger.error("Session unavailable.");
  }
  passport.authenticate(request.session?.provider, (error, user) => {
    if (error) {
      logger.error("Couldn't verify profile.", { error });
      response.locals.view.errors.push("Auth couldn't be started.");
      return response.render("sign-in", response.locals.view);
    }
    if (!user) {
      logger.warn("The provided profile isn't linked to a user.");
      response.locals.view.errors.push("Credential hasn't access.");
      return response.render("sign-in", response.locals.view);
    }
    request.login(
      user,
      error => login(error, user, request, response) as unknown
    );
  })(request, response, next);
});

router.get(
  "/sign-in/:provider",
  (request: express.Request, response: express.Response, next) => {
    if (request.session) {
      if (request.query.returnUrl) {
        request.session.returnUrl = request.query.returnUrl;
      }
      request.session.provider = request.params.provider;
    }
    passport.authenticate(request.params.provider, {
      scope: "openid profile email"
    })(request, response, next);
  }
);

router.get("/sign-in", viewify, userify, (request, response) => {
  if (request.user) {
    return response.redirect("/");
  }
  response.render("sign-in", response.locals.view);
});

router.get(
  "/sign-out",
  getAfterSignOutUrl,
  wrap(async (request: express.Request, response: express.Response) => {
    try {
      request.logout();
      await new Promise(resolve =>
        request.session ? request.session.destroy(resolve) : resolve()
      );
      response.clearCookie(session.options.name, session.options.cookie);
    } catch (error) {
      logger.error("Couldn't sign out.", { error });
    }
    response.redirect(response.locals.afterSignOutUrl || "/");
  })
);

export { router as auth, authentify, viewify, userify };
