import * as express from "express";
import * as expressSession from "express-session";

import logger from "../logger";
import { session } from "../session";

import { auth, viewify, userify } from "./auth";
import { docroot } from "./docroot";
import { chat } from "./chat";

class Routes {
  public router = express.Router(); /* eslint-disable-line new-cap */

  build(): void {
    this.router.use(express.static("dist/static"));
    this.router.use(
      express.json({
        type: ["application/json", "application/csp-report"]
      })
    );
    this.router.use(express.urlencoded({ extended: true }));
    session.plug();
    this.router.use(expressSession(session.options));

    this.router.use(auth);
    this.router.use(docroot);
    this.router.use(chat);

    this.router.use(viewify, userify, (request, response) => {
      logger.warn("Route not found.", {
        request: { method: request.method, originalUrl: request.originalUrl }
      });
      response.status(404).render("error/404", response.locals.view);
    });
    this.router.use(
      viewify,
      userify,
      (
        error: Error,
        request: express.Request,
        response: express.Response,
        next: express.NextFunction
      ) => {
        logger.error("Route unhandled error.", {
          request: {
            method: request.method,
            originalUrl: request.originalUrl,
            body: request.body
          },
          error
        });
        if (response.headersSent) {
          return next(error);
        }
        response.status(500).render("error/500", response.locals.view);
      }
    );
  }
}

const routes = new Routes();
export default routes;
