import * as express from "express";

import { authentify, viewify } from "./auth";
import wrap from "../routes/wrap";
import logger from "../logger";
import { daos, ChatMessage, ChatRoom, User } from "../daos";
import io from "../io-server";
import amqpServer from "../amqp-server";
import { Options } from "../options";

const router = express.Router(); /* eslint-disable-line new-cap */

interface Payload {
  httpCode: number;
  errors: string[];
}

interface GetRoomsPayload extends Payload {
  rooms: ChatRoom[];
}

interface GetChatPayload extends Payload {
  messages: ChatMessage[];
}

// TODO: This function needs serious refactoring.
function fork(
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
): express.Response | void {
  const payload: Payload = {
    httpCode: 200,
    errors: []
  };
  let text: string = request.body.text ?? "";
  text = text.trim();
  if (!text) {
    return response.json(payload);
  }
  if (text.startsWith("/stock=")) {
    try {
      const stock = text.slice(7);
      if (stock.length <= 0) {
        payload.httpCode = 400;
        payload.errors.push(
          "A ticker needs to be specified for the 'stock' command, e.g. AAPL.US, TSLA.US, etc."
        );
        return response.json(payload);
      }
      amqpServer.enqueue(
        Options.CloudAMQP.StockQueue,
        Buffer.from(JSON.stringify({ room: request.body.room, stock: stock }))
      );
      return response.json(payload);
    } catch (error) {
      logger.error("Couldn't enqueue command.", error);
      payload.httpCode = 500;
      payload.errors.push("Couldn't send command. Try again.");
    }
    return response.json(payload);
  }
  next();
}

router.get(
  "/room",
  wrap(async (_, response) => {
    const payload: GetRoomsPayload = {
      httpCode: 200,
      errors: [],
      rooms: []
    };
    try {
      payload.rooms = await daos.room.getAll();
    } catch (error) {
      logger.error("Couldn't load rooms.", error);
      payload.httpCode = 500;
      payload.errors.push("Couldn't load rooms. Refresh the page.");
    }
    return response.json(payload);
  })
);

router.get(
  "/chat/:room",
  wrap(async (request, response) => {
    const payload: GetChatPayload = {
      httpCode: 200,
      errors: [],
      messages: []
    };
    try {
      payload.messages = await daos.chat.getByRoom(request.params.room);
    } catch (error) {
      logger.error("Couldn't load messages.", error);
      payload.httpCode = 500;
      payload.errors.push("Couldn't load messages. Refresh the page.");
    }
    return response.json(payload);
  })
);

router.post(
  "/chat",
  viewify,
  authentify,
  fork,
  wrap(async (request, response) => {
    const payload: Payload = {
      httpCode: 200,
      errors: []
    };
    try {
      const user = request.user as User;
      const message: ChatMessage = {
        text: request.body.text,
        room: request.body.room,
        user: { _id: user._id, handle: user.handle }
      };
      const result = await daos.chat.insert(message);
      if (!!result) {
        // TODO: Use real rooms.
        io.emit(message.room, result);
      } else {
        payload.httpCode = 500;
        payload.errors.push("Couldn't send the message. Try again.");
      }
    } catch (error) {
      logger.error("Couldn't insert message.", error);
      payload.httpCode = 500;
      payload.errors.push("Couldn't send the message. Try again.");
    }
    return response.json(payload);
  })
);

export { router as chat };
