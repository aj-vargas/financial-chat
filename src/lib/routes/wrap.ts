import * as express from "express";

type AsyncMiddleware = (
  request: express.Request,
  response: express.Response,
  next: express.NextFunction
) => Promise<express.Response | void>;

type MiddlewareArgsRest = [
  express.Request,
  express.Response,
  express.NextFunction
];

function wrap(fn: AsyncMiddleware) {
  return function(...args: MiddlewareArgsRest): unknown {
    return fn(...args).catch(args[2]);
  };
}

export default wrap;
