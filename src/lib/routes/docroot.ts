import * as express from "express";

import { viewify, userify } from "./auth";
import logger from "../logger";

const router = express.Router(); /* eslint-disable-line new-cap */

router.get("/", viewify, userify, (_, response) => {
  response.render("index", response.locals.view);
});

router.post("/csp-report", (request, response) => {
  logger.info("CSP report.", { request: { body: request.body } });
  response.status(204).end();
});

export { router as docroot };
