import { daos, ChatMessage } from "./daos";
import { Options } from "./options";
import logger from "./logger";
import botPool from "./bot-pool";
import ioServer from "./io-server";
import httpServer from "./http-server";
import amqpServer from "./amqp-server";

const state = {
  enteredClean: false
};

function emitToRoom(message: ChatMessage): void {
  ioServer.emit(message.room, message);
}

export async function clean(): Promise<void> {
  if (state.enteredClean) {
    return;
  }
  state.enteredClean = true;
  logger.info("Cleaning...");
  botPool.kill();
  await ioServer.disconnect();
  await httpServer.disconnect();
  await amqpServer.disconnect();
  await daos.disconnect();
  logger.info("Bye.");
}

export async function main(): Promise<void> {
  try {
    logger.info("Starting...");
    await daos.connect();
    await amqpServer.connect([
      Options.CloudAMQP.StockQueue,
      Options.CloudAMQP.AnswerQueue
    ]);
    amqpServer.dequeue(Options.CloudAMQP.AnswerQueue, emitToRoom);
    httpServer.connect();
    ioServer.connect();
    botPool.fill();
    logger.info("Ready.");
  } catch (error) {
    logger.error("Couldn't init.", error);
    clean();
  }
}
