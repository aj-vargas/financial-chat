import { inspect } from "util";
import * as moment from "moment-timezone";
import * as winston from "winston";

import { getFilename } from "./shared";
import { Options } from "./options";

class Logger {
  private logger: winston.Logger | undefined;
  private name: string | undefined;
  private stamped: boolean | undefined;

  constructor(name?: string, stamped?: boolean) {
    this.name = name;
    this.stamped = stamped;
  }

  // Logger creation is here instead of the constructor so that the module keeps
  // its reusability WITHOUT creating an extra transport when loaded by multiple
  // scripts as it would happen with the logger instantiation used as default
  // export.
  create(): void {
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.metadata(),
        winston.format.printf(info => {
          const timestamp = moment()
            .tz(Options.LogTimezone)
            .format();
          const level = info.level.toUpperCase();
          // Show hidden is set to false, which could discard several
          // properties, e.g. MongoDB error.message which is hidden.
          const metadata =
            Object.keys(info.metadata).length > 0
              ? "\n" + inspect(info.metadata, { showHidden: false, depth: 3 })
              : "";
          return `${timestamp} ${level} ${info.message}${metadata}`;
        })
      ),
      transports: [
        new winston.transports.File({
          filename: getFilename(
            ".log",
            this.name,
            this.stamped ? Options.LogTimezone : undefined
          )
        })
      ]
    });
  }

  /*
   * When logging with winston, the metadata `meta` is wrapped in an object
   * because if it has a `message` property it will overwrite the message
   * set by the parameter `message`.
   */

  info(message: string, meta?: object): void {
    if (!this.logger) {
      this.create();
    }
    this.logger?.info(message, meta && { meta });
  }

  warn(message: string, meta?: object): void {
    if (!this.logger) {
      this.create();
    }
    this.logger?.warn(message, meta && { meta });
  }

  error(message: string, meta?: object): void {
    if (!this.logger) {
      this.create();
    }
    this.logger?.error(message, meta && { meta });
  }
}

export default new Logger();
export { Logger };
