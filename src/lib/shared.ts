import * as path from "path";
import * as moment from "moment-timezone";

export function getFilename(
  extension: string,
  name?: string,
  tzStamped?: string
): string {
  const filename = name ?? require.main?.filename ?? "unnamed";
  const formatOptions: path.FormatInputPathObject = {
    name: path.parse(filename).name
  };
  if (extension) {
    formatOptions.ext = extension;
  }
  if (tzStamped) {
    formatOptions.name +=
      "_" +
      moment()
        .tz(tzStamped)
        .format();
  }
  return path.format(formatOptions);
}
