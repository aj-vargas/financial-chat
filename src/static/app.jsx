// TODO: Remove hardcoded port.
const socket = io("http://localhost:49159/financial-chat");

function FinancialChat() {
  const [redirectTo, setRedirectTo] = React.useState("");
  const [errors, setErrors] = React.useState([]);
  const [rooms, setRooms] = React.useState([]);
  const [room, setRoom] = React.useState("");
  const [text, setText] = React.useState("");
  const [chat, setChat] = React.useState([]);

  React.useEffect(() => {
    requestRooms();
  }, []);

  function onChangeText(event) {
    setText(event.target.value);
  }

  function onRoomMessage(message) {
    setChat(oldChat => [...oldChat, message]);
  }

  async function onChangeRoom(event) {
    try {
      const target = event.target.value;
      if (!target || !target.trim()) {
        setChat([]);
        setRoom("");
        socket.off();
        setErrors([]);
        return;
      }
      const messages = await requestRoomChat(target);
      if (!messages) {
        return;
      }
      socket.off();
      setChat(messages);
      setRoom(target);
      socket.on(target, onRoomMessage);
      setErrors([]);
    } catch (error) {
      setErrors(["Couln't load room chat. Select the room again."]);
    }
  }

  async function onSendText() {
    try {
      if (!room || !room.trim()) {
        setErrors(["You need to be in a room to send a message."]);
        return;
      }
      if (!text || !text.trim()) {
        setErrors(["Nothing to send."]);
        setText("");
        return;
      }
      const wasSent = await requestSendText();
      if (!wasSent) {
        return;
      }
      setText("");
      setErrors([]);
    } catch (error) {
      setErrors(["Couln't send message. Try again."]);
    }
  }

  async function requestRooms() {
    try {
      const response = await (
        await fetch("/room", {
          method: "GET",
          headers: { Accept: "application/json" }
        })
      ).json();
      if (response.errors.length) {
        setErrors(response.errors);
      } else {
        setRooms(response.rooms);
      }
    } catch (error) {
      if (error.message) {
        setErrors([error.message]);
      } else {
        setErrors(["Couln't load rooms. Refresh the page."]);
      }
    }
  }

  async function requestRoomChat(target) {
    const response = await (
      await fetch(`/chat/${target}`, {
        method: "GET",
        headers: { Accept: "application/json" }
      })
    ).json();
    if (response.errors.length) {
      setErrors(response.errors);
      return;
    }
    return response.messages;
  }

  async function requestSendText() {
    let response = await fetch("/chat", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ text, room })
    });
    response = await response.json();
    if (response.redirectTo) {
      setErrors([]);
      setRedirectTo(response.redirectTo);
      return false;
    }
    if (response.errors.length) {
      setErrors(response.errors);
      return false;
    }
    return true;
  }

  return (
    <div>
      {errors.map(error => (
        <p key={error}>{error}</p>
      ))}
      {redirectTo && (
        <p>
          Expired session. You need to sign in. Click{" "}
          <a href={redirectTo}>here</a>.
        </p>
      )}
      <select value={room} onChange={onChangeRoom}>
        <option value="">select a room</option>
        {rooms.map(room => (
          <option value={room.name} key={room._id}>
            {room.name}
          </option>
        ))}
      </select>
      <div className="chat__messages">
        {chat.map(message => (
          <div key={message._id} className="chat__message">
            <div className="chat__handle">{message.user.handle}</div>
            <div>{message.text}</div>
          </div>
        ))}
      </div>
      <input
        value={text}
        onChange={onChangeText}
        onKeyPress={e => e.key === "Enter" && onSendText()}
        placeholder="enter a message or command"
        className="chat__text"
      />
      <button onClick={onSendText}>Send</button>
    </div>
  );
}

ReactDOM.render(<FinancialChat />, document.getElementById("app"));
