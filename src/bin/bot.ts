#!/usr/bin/env node

import { Chance } from "chance";
import * as amqp from "amqplib";

import { daos } from "../lib/daos";
import { Logger } from "../lib/logger";
import { Options } from "../lib/options";

// TODO: Reconnect when the connection or channel is closed.

const state = {
  enteredClean: false,
  name: `${new Chance().name()} (bot)`,
  connection: (null as unknown) as amqp.Connection,
  channel: (null as unknown) as amqp.Channel
};

const logger = new Logger(state.name.toLowerCase().replace(/ /g, "-"));

function log(error: Error): void {
  logger.error("Bot AMQP error.", error);
}

function sendAnswer(room: string, answer: string): void {
  state.channel?.sendToQueue(
    Options.CloudAMQP.AnswerQueue,
    Buffer.from(
      JSON.stringify({
        _id: daos.getObjectId(),
        room,
        user: { handle: state.name },
        text: answer,
        createdAt: Date.now()
      })
    )
  );
}

async function consume(message: amqp.ConsumeMessage | null): Promise<void> {
  let request = null;
  try {
    const content = message?.content.toString();
    if (!content) {
      throw new Error("No content.");
    }
    request = JSON.parse(content);
    const candle = await daos.stooq.getCandle(request.stock);
    const answer = `'${request.stock}' quote is $${candle.close} per share.`;
    sendAnswer(request.room, answer);
    if (message) {
      state.channel?.ack(message);
    }
  } catch (error) {
    logger.error("Couldn't consume message.", error);
    if (request.room) {
      sendAnswer(
        request.room,
        `The quote for '${request.stock}' couldn't be obtained. Try again.`
      );
    }
    if (message) {
      state.channel?.nack(message, false, false);
    }
  }
}

export async function clean(): Promise<void> {
  if (state.enteredClean) {
    return;
  }
  state.enteredClean = true;
  state.channel.off("error", log);
  state.connection.off("error", log);
  await state.connection.close();
  logger.info("Cleaning...");
  logger.info("Bye.");
}

export async function main(): Promise<void> {
  try {
    logger.info("Starting...");
    state.connection = await amqp.connect(Options.CloudAMQP.Url);
    state.channel = await state.connection.createChannel();
    await state.channel.assertQueue(Options.CloudAMQP.StockQueue, {
      durable: false
    });
    await state.channel.assertQueue(Options.CloudAMQP.AnswerQueue, {
      durable: false
    });
    state.connection.on("error", log);
    state.channel.on("error", log);
    state.channel.consume(
      Options.CloudAMQP.StockQueue,
      msg => consume(msg) as unknown,
      {
        noAck: false
      }
    );
    logger.info("Ready.");
  } catch (error) {
    logger.error("Couldn't init.", error);
    clean();
  }
}

process.on("SIGINT", () => clean() as unknown);
process.on("SIGTERM", () => clean() as unknown);

main();
