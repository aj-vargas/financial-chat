#!/usr/bin/env node

import * as lib from "../lib";

process.on("SIGINT", () => lib.clean() as unknown);
process.on("SIGTERM", () => lib.clean() as unknown);

lib.main();
