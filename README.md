# financial-chat

## Instructions

To execute the server and the bot(s) run the following commands in a shell:

```bash
git clone https://gitlab.com/aj-vargas/financial-chat.git
cd financial-chat
# place the `.env` in this folder, the file was attached to the email sent with the repository link
npm install
npm run build
npm run debug
```

Then visit `HTTP_URL` set in `.env`. You will be able to see the rooms and the messages in each one, but you won't be able to send messages until you sign in. To sign in click in the `Sign In` link. If you don't have an account you can create one in the page to which you were redirected after clicking `Sign In`, click on `Sign Up`.

Note that the npm scripts were tested in *nix systems, their behaviour is untested in Microsoft Windows.

Multiple logs will be generated in the project root, one for the `financial-chat.ts` server and one for each instantiated bot.

## Bots

The server (`financial-chat.ts`) starts multiple bot instances (as many as `TOTAL_BOTS` in `.env` says), i.e. `bot.ts` isn't only decoupled from the server code but also each bot instance runs in its own separate process, each with its own name which is reflected in the chat rooms.

This also means that when the server isn't properly shut down, the bot instances will be left running. To kill them, identify the bot PID using something like `ps aux | grep bot.js` or search the PIDs in `financial-chat.log` and use `kill` on the corresponding PIDs.

If you want to start a bot manually, even if the server is already up, do `node dist/bin/bot.js` in the project root. Note that when started manually you will need to kill the bot when you no longer need it, as they aren't registered in the server's bot pool.

## TODOs

- [x] Allow registered users to sign in and chat (using Auth0).
- [x] Allow users to post commands to a message broker (RabbitMQ through CloudAMQP).
- [x] Create a decoupled bot to call an API and respond to commands.
- [x] Have chats messages ordered and show the last 50 (this and other data is stored in MongoDB Atlas).
- [x] Have more than one chatroom.
- [x] Unit tests (x2) made with Jest (run `npm test`).
- [x] Handle errors within the bot.

## Notes

- The initial page load may be slow, this is because the frontend app (React) is compiled in the browser instead of being served precompiled. In most real projects the frontend app would sit in its own repository and be served by a CDN.
- The bot will respond to the commands in the room where the command was sent by the user, not where the user currently is.
